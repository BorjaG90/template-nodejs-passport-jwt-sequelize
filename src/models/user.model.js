const Sequelize = require('sequelize');
const db = require('../config/database');

const User = db.define('user', {
  id: { type: Sequelize.BIGINT, primaryKey: true },
  email: { type: Sequelize.STRING },
  password: { type: Sequelize.STRING },
  username: { type: Sequelize.STRING },
  firstname: { type: Sequelize.STRING },
  lastname: { type: Sequelize.STRING },
  enabled: { type: Sequelize.BOOLEAN },
});

module.exports = User