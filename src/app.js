'use strict';

require('dotenv').config();
const express           = require('express');
const bodyParser        = require('body-parser');
const passport          = require("passport");
const errorMdw         = require('./middleware/error');


let app = express();

// passport config
require('./config/passport')(passport);

//conectamos todos los middleware de terceros
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.json());
app.use(passport.initialize());
app.use('/public', express.static(process.cwd() + '/public'));

// Database
const db = require('./config/database');

// Test DB
db.authenticate()
  .then(() => console.log('Database connected...'))
  .catch(err => console.log('Error: ' + err))

//Routes
app.use('/api', require('./routes/user.routes'));

//el último nuestro middleware para manejar errores
app.use(errorMdw.errorHandler);
app.use(errorMdw.notFoundHandler);

let port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log('express server listening ...');
});

