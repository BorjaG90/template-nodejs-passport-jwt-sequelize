"use strict";

const express = require("express");
const router = express.Router();
const authMdw = require("../middleware/auth");
const SampleController = require("../controllers/sample");
const UserController = require("../controllers/user.controller");

router.post("/login", UserController.login);
router.post("/register", UserController.register);

router.get("/test", SampleController.unprotected);
router.get(
  "/protected",
  authMdw.ensureAuthenticated,
  SampleController.protected
);

module.exports = router;
