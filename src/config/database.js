const { database } = require("./keys");
const Sequelize = require("sequelize");

module.exports = new Sequelize(
  database.database,
  database.user,
  database.password,
  {
    host: database.host,
    dialect: "mysql",
    operatorAliases: false,

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  }
);