# Plantilla Nodejs Passport JWT Sequelize

![Template](https://img.shields.io/badge/Project-Template-black.svg)
![](https://img.shields.io/badge/Status-active-green.svg)
![](https://img.shields.io/badge/Maintained-yes-green.svg)

Plantilla de creación de un proyecto en NodeJS con autenticación Passport y JWT y ORM Sequelize

## Features
- Autenticación en passport-local y passport-jwt
- Registro y Login de Usuarios

## Usage
    Requiere de la creación un fichero .env como el de la plantilla incluida y debe ser rellenado con los datos y claves necesarios
    
    Ejecutar npm i

## Built With
<a href="https://www.javascript.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/javascript.jpeg" width=50 alt="JavaScript"></a>
<a href="https://nodejs.org/es/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/nodejs.png" width=50 alt="NodeJS"></a>
<a href="https://expressjs.com/es/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/expressJS.png" width=50 alt="ExpressJS"></a>

### Platforms
<a href="https://code.visualstudio.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/vscode.png" width=50 alt="VSCode"></a>

## Author

- **Borja Gete** - <borjag90dev@gmail.com>